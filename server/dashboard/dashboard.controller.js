'use strict';

var passport = require('passport');
var config = require('../config/environment');
var jwt = require('jsonwebtoken');


/**
 * Get into the admin dashboard
 * restriction: 'user'
 */
exports.dashboard = function(req, res) {
  res.send('Hello World dashboard');
};
