'use strict';

var express = require('express');
var passport = require('passport');
var config = require('../config/environment');
var auth = require('../auth/auth.service');
var controller = require('./dashboard.controller');

var router = express.Router();

// define the about route  router.get('/', auth.hasRole('user'), controller.index);
router.get('/dashboard',auth.hasRole('admin'),controller.dashboard);

module.exports = router;